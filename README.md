# Aplicativo Maratonei


**Progresso:** CONCLUÍDO<br />
**Autor:** Paulo Victor de Oliveira Leal<br />
**Data:** 2017<br />

### Objetivo
Implementar um aplicativo para consumir a API do Maratonei e exibir as séries e os valores calculados pela otimização do Simplex

### Observação

IDE:  [Visual Studio 2017](https://visualstudio.microsoft.com/)<br />
Linguagem: [C#](https://visualstudio.microsoft.com/xamarin/)<br />
Banco de dados: [SQLite](https://www.sqlite.org/)<br />

### Execução

No Visual Studio
    

### Contribuição

Esse projeto está finalizado e livre para uso.

## Licença
<!---

[//]: <> (The Laravel framework is open-sourced software licensed under the [MIT license]https://opensource.org/licenses/MIT)

-->